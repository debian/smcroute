smcroute (2.5.7-1) unstable; urgency=medium

  * New upstream version 2.5.7.
  * Package is compliant to Debian policy 4.7.0 (no changes needed).
  * d/control: Switch build-dependencies from pkg-config to pkgconf.
  * Auto-generate manpage for smcroute command using help2man.

 -- Micha Lenk <micha@debian.org>  Sun, 12 May 2024 14:59:40 +0200

smcroute (2.5.6-3) unstable; urgency=medium

  * Switch build dependency from systemd to systemd-dev (Closes: #1060531).
  * debian/copyright: Drop unreferenced paragraph for BSD-4-clause license.

 -- Micha Lenk <micha@debian.org>  Thu, 22 Feb 2024 21:19:57 +0100

smcroute (2.5.6-2) unstable; urgency=medium

  [ Micha Lenk ]
  * Drop dependency on lsb-base (>= 3.0-6). Thanks to Lintian for flagging this
    as obsolete.

  [ Luca Boccassi ]
  * Non-maintainer upload.
  * autopkgtest: drop is-system-running check (Closes: #1052548)

 -- Micha Lenk <micha@debian.org>  Mon, 25 Sep 2023 23:58:00 +0200

smcroute (2.5.6-1) unstable; urgency=medium

  * New upstream version 2.5.6.
  * debian/watch: Switch to Github API to search for new versions.
  * Package is compliant to Debian policy 4.6.2 (no changes needed).
  * d/copyright: Drop stanza for no longer distributed src/ip_mroute.h and
    update copyright.

 -- Micha Lenk <micha@debian.org>  Sun, 08 Jan 2023 14:02:40 +0100

smcroute (2.5.5-1) unstable; urgency=medium

  * New upstream version 2.5.5.

 -- Micha Lenk <micha@debian.org>  Mon, 22 Nov 2021 20:38:19 +0100

smcroute (2.5.3-2) unstable; urgency=medium

  * debian/tests: Re-work daemon-init-scripts to make PID lookup more reliable.
  * debian/rules: Don't ship the smcroute-helper.service when building without
    systemd support (i.e. kFreeBSD as of now).
  * debian/upstream: Add metadata.

 -- Micha Lenk <micha@debian.org>  Sun, 26 Sep 2021 21:09:52 +0200

smcroute (2.5.3-1) unstable; urgency=medium

  * New upstream version 2.5.3.
  * Drop patch sd_notify.patch (applied upstream).
  * debian/watch: Update URLs to match changes on github.com.
  * debian/source/options: Ignore generated files in local change detection.
  * Package is compliant to Debian policy 4.6.0 (no changes needed).
  * Re-enable builds on kFreeBSD by conditionally disabling systemd
    integration, use of libcap and multicast router discovery on kFreeBSD.
  * debian/tests: Fix race condition in daemon-init-scripts test.

 -- Micha Lenk <micha@debian.org>  Fri, 24 Sep 2021 11:14:34 +0200

smcroute (2.4.4-3) unstable; urgency=medium

  * debian/tests: Skip multicast cache tests on systems without multicast
    capable interfaces.

 -- Micha Lenk <micha@debian.org>  Mon, 01 Mar 2021 09:22:50 +0100

smcroute (2.4.4-2) unstable; urgency=medium

  * debian/changelog: Mark bug #952418 as closed in 2.4.4-1
  * Add sd_notify.patch teaching smcrouted to call sd_notify() when up and
    running. This fixes the systemd race condition that causes CI tests to fail
    on slow systems (closes: #924361).
  * autopkgtests: daemon-init-scripts: Revert the sleeps that were trying to
    "make tricky operations succeed". This is now fixed properly (see above).
  * Add build dependency on libsystemd-dev.

 -- Micha Lenk <micha@debian.org>  Sun, 28 Feb 2021 16:30:39 +0100

smcroute (2.4.4-1) unstable; urgency=medium

  [ Adrien ]
  * Remove invalid comma in "After" list of systemd service file (closes:
    #952418).

  [ Micha Lenk ]
  * New upstream version 2.4.4.
    - Drop all patches (all applied upstream).
  * Switch to current Salsa CI Team pipeline.
  * Package is compliant to Debian policy 4.5.1.
  * Make myself maintainer and move Joachim to uploaders.
  * Switch to debhelper compat level 13 and get rid of debian/compat.
  * debian/rules: No longer explicitly enable autoreconf and systemd debhelper
    sequence. They are enabled by default since debhelper 10 anyways.
  * debian/control: Add Pre-Depends as advised by Lintian (tag
    skip-systemd-native-flag-missing-pre-depends).
  * debian/rules: Wrap configure flags into individual lines for better
    tracking of changes in Git.
  * Re-work and streamline init script handling.
  * autopkgtests:
    - Drop isolation-machine restriction.
    - daemon-init-scripts: Try to make tricky operations succeed.

 -- Micha Lenk <micha@debian.org>  Thu, 25 Feb 2021 13:28:54 +0100

smcroute (2.4.2-4) unstable; urgency=medium

  * Import upstream fix for malfunctioning 'stop' command to init script,
    closes: #924044
  * Import upstream fix to allow same outbound interface as inbound when
    routing.  This is slightly hairy, but previous releases of SMCRoute
    supported this and the unit tests rely on this to work
  * Fix unit tests, with upstream and test setup fixes, closes: #921577
  * Bump Standards-Version to 4.3.0, no changes needed
  * Use debhelper maintscript to relocate old /etc/startup.sh script
    to /etc/smcroute.  Fixes an outstanding lintian warning

 -- Joachim Nilsson <troglobit@gmail.com>  Thu, 07 Mar 2019 06:40:19 +0100

smcroute (2.4.2-3) unstable; urgency=medium

  * Add missing Build-Depends for systemd, needed for unit file path
  * Update and reformat debian/copyright according to DEP-5, closes: #908701
  * Fix lintian man page error, too long line
  * debian/rules:
    + Enable hardening with bindnow, suggested by lintian
    + Export extra CFLAGS, to enable in build, drop -Wall already in upstream
  * debian/NEWS: Alert users of changes to .conf files since 2.0.0-6
  * smcroute-helper: Add documentation to unit file, reference NEWS.Debian
  * Enable privilege separation support

 -- Joachim Nilsson <troglobit@gmail.com>  Sun, 23 Sep 2018 22:40:04 +0200

smcroute (2.4.2-2) UNRELEASED; urgency=medium

  * Add libsystemd-dev and pkg-config to Build-Depends.
    SMCRoute has native support for installing the systemd unit file.
    However, to activate this support the system needs both pkg-config
    and libsystemd-dev.
    This change fixes the following lintian error found by Micha Lenk when
    building in a clean sid chroot:
        E: smcroute: omitted-systemd-service-for-init.d-script smcroute
  * Enable privilege separation, add libcap-dev to Build-Depends.
    Since SMCRoute v2.2 privilege separation using libcap is supported.  To
    enable this, and silence the big warning at configure time, we need to
    have libcap-dev (and pkg-config) installed at build time.
  * Enforce use of systemd and libcap in configure script

 -- Joachim Nilsson <troglobit@gmail.com>  Fri, 14 Sep 2018 18:52:04 +0200

smcroute (2.4.2-1) unstable; urgency=medium

  [ Micha Lenk ]
  * Update Git repository for Debian packaging repo

  [ Joachim Nilsson ]
  * New upstream version 2.4.1
  * Remove local patches included in latest imported upstream
  * Skip removal of upstream ChangeLog, no longer installed in doc
  * Fix lintian warning unnecessary-testsuite-autopkgtest-field
    Drop 'Testsuite:' from debian/control, fixes this lintian warning
        W: smcroute source: unnecessary-testsuite-autopkgtest-field
  * Add Conflicts with mrouted, pimd, and quagga-pimd
    They all share the same multicast routing socket, and only one daemon
    at a time can do MRT_INIT
  * Update package description, SCMRoute is not just limited to Linux
    SMCRoute is not limited to Linux, it works also on FreeBSD and NetBSD
  * Add systemd support, unit file included by upstream
    debhelper >= 9.20160709 includes dh-systemd
  * Remove Debian specific /etc/smcroute/startup.sh script
    We should recommend smcroute.conf over the legacy startup script.
    However, we still need to honor any already installed script.
  * New maintainer, closes: #906889
  * Fix lintian warning; old debhelper version, >= 10 to support Stretch
  * Bump Standards-Version to 4.2.1 after audit
    https://www.debian.org/doc/packaging-manuals/upgrading-checklist.txt
    As far as I can see, no changes are needed for SMCRoute
  * debian/changelog: Remove empty line at end of file
  * New upstream version 2.4.2, closes: #846780
  * Import upstream spelling fixes, found by lintian
  * Import upstream fix for missing status command in SysV init script
  * Enable SMCRoute mrdisc feature, RFC4286
    This change enables the SMCRoute built-in support for RFC4286, multicast
    router discovery protocol.  By default SMCRoute does nothing, but when
    it is enabled on a phyint SMCRoute periodically sends discovery frames
  * Add SMCRoute systemd helper service, runs /etc/smcroute/startup.sh
  * Strip dh-autoreconf from Build-Depends, default in debhelper >= 10
    Fixes the following lintian warning
        W: smcroute source: useless-autoreconf-build-depends dh-autoreconf

 -- Joachim Nilsson <troglobit@gmail.com>  Mon, 10 Sep 2018 21:44:17 +0200

smcroute (2.0.0-6) unstable; urgency=medium

  * Trim overlong changelog line in previous entry (thanks Lintian).
  * Change priority to 'optional' to make it compliant to policy 4.0.1
  * Add dependency on lsb-base (>= 3.0-6) as the init script sources
    /lib/lsb/init-functions. Thanks to Lintian for flagging this error.
  * Add cherry-picked upstream patch to make the build-id optional
    (debian/patches/0002-Make-output-of-build-id-optional.patch) so that the
    Debian builds become reproducible (closes: #784694).

 -- Micha Lenk <micha@debian.org>  Tue, 08 Aug 2017 21:17:44 +0200

smcroute (2.0.0-5) unstable; urgency=medium

  * Move /etc/startup.sh to /etc/smcroute/startup.sh where it is expected by
    the init script (closes: #782826). Also add postinst, postrm and preinst
    scripts invoking dpkg-maintscript-helper to correctly move any
    /etc/startup.sh modified by administrators.
  * debian/rules: delete duplicate changelog file ChangeLog.gz.

 -- Micha Lenk <micha@debian.org>  Tue, 05 May 2015 21:49:51 +0200

smcroute (2.0.0-4) unstable; urgency=medium

  * Fix autopkgtest: Add missing smcroute test dependency, missing comma, and
    missing allow-stderr (Closes: #767592). Thanks to Martin Pitt
    <martin.pitt@ubuntu.com> for providing the patch.

 -- Micha Lenk <micha@debian.org>  Sat, 01 Nov 2014 20:55:08 +0100

smcroute (2.0.0-3) unstable; urgency=medium

  * Add Testsuite: autopkgtest to debian/control, on behalf of a traveling
    Micha

 -- Joachim Breitner <nomeata@debian.org>  Thu, 30 Oct 2014 16:11:03 +0100

smcroute (2.0.0-2) unstable; urgency=medium

  * Delete debian/smcroute.dirs because package builds also without it.
  * Install binaries to /usr/sbin to make init script working again
    (Closes: #766915).
  * Add patch 0001-Fix-issue-10-Loop-forever-on-command-if-daemon-not-r.patch
    to fix hanging init script on stop in case the daemon isn't running.
  * Add some Autopkgtests covering daemon start stop via System V init scripts,
    addition and removal of static multicast routes.

 -- Micha Lenk <micha@debian.org>  Thu, 30 Oct 2014 00:27:59 +0100

smcroute (2.0.0-1) unstable; urgency=low

  * New upstream version, does build again on FreeBSD (closes: #763381).
  * Use init script provided by upstream.

 -- Micha Lenk <micha@debian.org>  Fri, 03 Oct 2014 10:44:16 +0200

smcroute (1.99.2-1) unstable; urgency=medium

  * New upstream version.
  * Drop patch 01_fix_segfault.patch, included upstream.
  * Update upstream homepage
  * Update debian/watch file for new upstream download location
  * Add Vcs-Browser: and Vcs-Git: control fields
  * Drop uploaders because Julien Blache retired from the Debian project.
    Thanks to Ana Guerrero for the hint (Closes: #688547)
  * debian/rules: Rewrite using dh makefile style, requiring Debhelper 9
  * Rewrite init script to use /lib/lsb/init-functions functions for output
  * Package is compliant to Debian Policy 3.9.6 (no changes needed)

 -- Micha Lenk <micha@debian.org>  Sun, 28 Sep 2014 21:35:08 +0200

smcroute (0.95-2) unstable; urgency=low

  * Switch to dpkg-source 3.0 (quilt) format for easier patch maintenance.
  * Added patch 01_fix_segfault.patch to fix a NULL pointer dereferencing in
    interface vector initialization (closes: #707793, LP: #1043688).

 -- Micha Lenk <micha@debian.org>  Wed, 22 May 2013 13:41:51 +0200

smcroute (0.95-1) unstable; urgency=low

  * New upstream version, added support for FreeBSD
  * debian/control:
    - Changed priority from 'optional' to 'extra' (see #609864).
    - Bump Standards-Version to 3.9.2 (no changes).
    - Swapped Maintainer and Uploader.

  Fixed Lintian errors/warnings:
  * debian/smcroute.init: Added LSB dependencies on $remote_fs.
  * debian/rules: Added recommended build-arch and build-indep targets.

 -- Micha Lenk <micha@debian.org>  Mon, 08 Aug 2011 20:08:48 +0200

smcroute (0.94.1-1) unstable; urgency=low

  * New upstream release
    + fixed IPv6 related startup problem of smcroute (closes: #564881).
  * Added debian/watch file with new download location (closes: #554164).

 -- Micha Lenk <micha@debian.org>  Wed, 13 Jan 2010 12:17:30 +0100

smcroute (0.94-1) unstable; urgency=low

  * New upstream release, supports IPv6 now (closes: #551209).
  * Adapted debian/rules to new build system.
  * Dropped debian specific manpage for mcsender and install the one now
    provided by upstream.
  * debian/control:
    + Added myself to Uploaders
    + Added Homepage field
    + Bump Standards-Version to 3.8.3 (no changes).

 -- Micha Lenk <micha@debian.org>  Mon, 02 Nov 2009 09:54:51 +0100

smcroute (0.93d-4) unstable; urgency=low

  * debian/control:
    + Bump Standards-Version to 3.7.3 (no changes).

 -- Julien BLACHE <jblache@debian.org>  Sat, 01 Mar 2008 16:39:57 +0100

smcroute (0.93d-3) unstable; urgency=low

  * debian/control:
    + Do not mention the obsolete mrouted package in the long description,
      instead tell about pimd and xorp (closes: #439263).

 -- Julien BLACHE <jblache@debian.org>  Thu, 23 Aug 2007 20:20:39 +0200

smcroute (0.93d-2) unstable; urgency=low

  * debian/rules:
    + Do not ignore errors from make clean.
  * debian/compat, debian/control:
    + Upgrade to DH_COMPAT 5.
  * debian/smcroute.init:
    + Add LSB header.

 -- Julien BLACHE <jblache@debian.org>  Thu, 02 Aug 2007 18:48:42 +0200

smcroute (0.93d-1) unstable; urgency=low

  * New upstream release.
    + Patches dropped, merged upstream.
  * debian/control:
    + Drop dpatch build-dependency.
  * debian/copyright:
    + Update the copyright holders.

 -- Julien BLACHE <jblache@debian.org>  Sun,  9 Jul 2006 18:51:45 +0200

smcroute (0.92-3) unstable; urgency=low

  * Switch to dpatch.
  * debian/control:
    + Build-Depends on dpatch.
  * debian/rules:
    + Use dpatch.
  * debian/patches/01_build_fixes.dpatch:
    + Added; various build fixes (missing includes, namespace clashes, ...).
  * debian/patches/02_daemon_init.dpatch:
    + Added; call init functions in the parent so we can return an error code
      to the initscript when something goes wrong.

 -- Julien BLACHE <jblache@debian.org>  Sun,  4 Jun 2006 11:41:29 +0200

smcroute (0.92-2) unstable; urgency=low

  * src/smcroute.c, src/syslog.c:
    + Fixed smcroute so that an error code is returned if the daemon fails to
      start, instead of always returning 0. That way, the initscript can now
      fail.

 -- Julien BLACHE <jblache@debian.org>  Sun,  4 Jun 2006 10:34:02 +0200

smcroute (0.92-1) unstable; urgency=low

  * Initial release (closes: #369785).

 -- Julien BLACHE <jblache@debian.org>  Thu,  1 Jun 2006 13:09:15 +0200
